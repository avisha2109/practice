import { Injectable } from '@angular/core';
import * as _ from 'underscore'

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  getComments(){
    return [
      { id: 1, name: 'Rahul', time: '10:45am, 1/12/18', comment: 'abcjjd' },
      { id: 2, name: 'Rahul',time : '12:15pm, 16/10/18', comment: 'abcjjd'},
      { id: 3, name: 'Saket', time : '2:15pm, 1/8/18', comment: 'ahidhd'},
      { id: 4, name: 'Dinesh', time : '11:45am, 13/8/18', comment: 'hhdhidhih' },
      { id: 5, name: 'Ashish', time : '00:15am, 23/6/18', comment: 'josfolfl' },
    ]
  }


    

  
  constructor() { }
}
